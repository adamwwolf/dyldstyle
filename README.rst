*********
dyldstyle
*********

`dyldstyle` contains tools for wrangling bundles on macOS.

I'm developing it to help with KiCad packaging on macOS. KiCad has a main application bundle, but it also has application bundles for its subprograms that are symlinked into it. KiCad brings along a Python.framework (as recommended by Apple) as well as multiple other dependencies. KiCad uses CMake, and while fixup_bundle does a lot of the work, it doesn't do a perfect job of things like Python packaging.

While debugging a particular tricky additional integration, I created a tool to diagnose what was going on with the loader directives and dyld.

I would like this tool to stop existing.  It is certainly possible we'll be able to run fixup_bundle on a bundle including Python and everything will *just work* one day, and I would like to incorporate all of this fucntionality into KiCad using CMake.

Many thanks to Greg Neagle and https://github.com/gregneagle/relocatable-python.

Usage
#####

Install this package, probably not system-wide.  I like to use a tool like pipx.

Point wrangle-bundle at your bundle, and cross your fingers!

`wrangle-bundle --python-version 3.8 --fix path/to/KiCad.app`

Development
###########

There's a lot of ways to do this.

You may want to check out this repository, create a virtual environment, activate it, and install this into it using `pip -e`, like this::

    git clone https://gitlab.com/adamwwolf/dyldstyle.git
    cd dyldstyle
    python3 -m venv venv
    source venv/bin/activate
    pip install -e .

Now, make a branch, do whatever, and run tox to run the linting and unit tests.

Meta
####

* https://hynek.me/articles/sharing-your-labor-of-love-pypi-quick-and-dirty/
